# sbctl kernel hook

A kernel hook to automatically generate and sign Unified Kernel Images (UKI's) with [sbctl](https://github.com/Foxboron/sbctl) on adding/updating/removing kernels via the xbps package manager.

# NOTE
THESE SCRIPTS WERE WRITTEN TO WORK ON **VOID LINUX**. IT MAY OR MAY NOT WORK ON ANY OTHER PLATFORM.
-
It should however be relatively easy to port to different platforms as it are just a bunch of shell scripts to be executed with certain parameters upon updating kernel packages.

## Installation
First make sure you have sbctl installed:
```
$ xbps-install -Sy sbctl
```
Then clone this repo and copy the necessary files:
```
$ git clone https://codeberg.org/mathijs/sbctl-kernel-hook.git
$ cd sbctl-kernel-hook
# cp -rv etc/ /
```


## Configuration
Configuration values are specified in `/etc/default/sbctl-kernel-hook`:

- `ESP`: path where the EFI System Partition is mounted;
- `EFI_STUB`: path of the efi stub to be used;
- `KERNEL`: path of the kernel to be used;
- `INITRAMFS`: path of the corresponding ramdisk;
- `CMDLINE`: path of the file containing the cmdline to be used;
- `UKI_OUT`: path to write the signed UKI to.
- `INTEL_UCODE` (optional): path of the intel microcode image that should be used;
- `AMD_UCODE` (optional): path of the amd microcode image that should be used;
- `OS_RELEASE` (optional): path of the file identifying the os the kernel is configured to boot (usually `/etc/os-release`). This is used by the bootloader to show for example: "Void Linux" instead of "vmlinuz-6.6.8_1";
- `SPLASH` (optional): splash screen to show during boot of the UKI. This is not the path of the kernel to be used;

## Secure boot keys
As this is a kernel hook for the `sbctl` program, there is to my knowledge (at the time of writing) no known way to change the hardcoded path to store the secure boot signing keys `/usr/share/secureboot/keys`. This is an `sbctl` issue, not one of this kernel hook.
